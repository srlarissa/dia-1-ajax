function handleClick(){
    const txtArea = document.querySelector('#msg');
    const history = document.querySelector('.msg-history');
    const li = document.createElement("li");

    li.innerHTML = `
        <p class="msg-texto">${txtArea.value}</p>
        <span class="btn">
            <button class="btn-editar" onclick="editar(this)">Editar</button>
            <button class="btn-excluir" onclick="excluir(this)">Excluir</button>
        </span>
    `
    txtArea.value = "";
    history.appendChild(li);
    
}

function editar(elemento){
    const parentEdit = elemento.parentElement.parentElement;
    parentEdit.firstChild.nextSibling.contentEditable = true
    elemento.parentElement.remove()
    const btnConfirmar = document.createElement('button');
    btnConfirmar.innerText = 'Enviar'
    btnConfirmar.style.cssText = `
        width: 70px;
        margin:10px;
        background-color:green;
        color: white;
        padding: 5px;
        border-style:none;
    `
    parentEdit.appendChild(btnConfirmar)

    btnConfirmar.addEventListener('click', () => {
        parentEdit.firstChild.nextSibling.contentEditable = false;
        btnConfirmar.remove()

        const btnReturn = document.createElement('span')
        btnReturn.className = "btn"
        btnReturn.innerHTML = `
        <button class="btn-editar" onclick="editar(this)">Editar</button>
        <button class="btn-excluir" onclick="excluir(this)">Excluir</button>
        `
        parentEdit.appendChild(btnReturn)
    })
}

function excluir(elemento){
    const parentDelete = elemento.parentElement.parentElement;
    parentDelete.remove();
}

//Inicio do link API - APP de mensagens

const url = "https://treinamento-ajax-api.herokuapp.com/messages"